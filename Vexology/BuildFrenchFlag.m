%This program builds and displays a French Flag
clc
clear;
clear Screen;
Screen('Preference', 'SkipSyncTests', 1);

%Size of screen
width=1000;
height=1000;

%Color definitions in RGB
Blue=[0 0 255];
White=[255 255 255];
Red=[255 0 0];

%Building screen
ScreenSize=[0 0 width height];
w=Screen('OpenWindow',0,White,ScreenSize);

%initializing stripes
BlueStripe=[0 0 width/3 height];
WhiteStripe=[width/3 0 2*width/3 height];
RedStripe=[2*width/3 0 width height];

%displaying Flag
Screen('FillRect', w,Blue, BlueStripe);
Screen('FillRect', w , White, WhiteStripe);
Screen('FillRect', w , Red, RedStripe);
Screen('Flip', w);