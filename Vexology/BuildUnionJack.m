%This program builds and displays a Union Jack
clear;
clear Screen;
Screen('Preference', 'SkipSyncTests', 1);

%Size of screen
w=1500;
h=750;

%Color definitions in RGB
Blue=[0 0 255];
White=[255 255 255];
Red=[255 0 0];

%Building screen
ScreenSize=[0 0 w h];

%initializing stripes
WhiteX1=[0 (h-h/10);0 h; w/10 h; w h/10; w 0; (w-w/10) 0];
WhiteX2=[w/10 0; 0 0; 0 h/10; (w-w/10) h; w h; w (h-h/10)];

RedX1=[0 0; 0 h/20; w h; w (h-h/20)];
RedX2=[0 h; w/20 h; w 0; (w-w/20) 0];


HalfWCW = w/12; %This is Half the White Cross Width 
HalfRCW = w/20; %This is Half the Red Cross width

WhiteCross1 = [0 h/2-HalfWCW w h/2+HalfWCW];
WhiteCross2 = [w/2-HalfWCW 0 w/2+HalfWCW h];

RedCross1 = [0 h/2-HalfRCW w h/2+HalfRCW];
RedCross2 = [w/2-HalfRCW 0 w/2+HalfRCW h];



%Displaying Scotish Flag
window=Screen('OpenWindow',0,Blue,ScreenSize);
Screen('FillPoly', window,White, WhiteX1);
Screen('FillPoly', window,White, WhiteX2);

%Displaying Scottish and North Irish Flag
Screen('FillPoly', window, Red, RedX1);
Screen('FillPoly', window, Red, RedX2);

Screen('FillRect', window, White, WhiteCross1);
Screen('FillRect', window, White, WhiteCross2);
Screen('FillRect', window, Red, RedCross1);
Screen('FillRect', window, Red, RedCross2);

Screen('Flip', window);