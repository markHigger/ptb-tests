function [ CarPoints ] = InitializeBullet(originCenterx,  originCentery, height, length)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
CarPoints(1,1)= originCenterx-length/2;
CarPoints(1,2)= originCentery-height/2;
CarPoints(2,1)= CarPoints(1,1);
CarPoints(2,2)= originCentery+height/2;

theta=0;
interval=pi/20;
r=height/2;
coorIdx=3;
while theta <= pi
    CarPoints(coorIdx,1)= (sin(theta)*r) + originCenterx+length/2;
    CarPoints(coorIdx,2)= (cos(theta)*r) + originCentery;
    
    theta=theta+interval;
    coorIdx=coorIdx+1;
    


end

