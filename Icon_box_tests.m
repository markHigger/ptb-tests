%% input parameters
clearvars; clc; sca;

%% Parameters
screenNumber = 0;
initIconBoxPos = [.1, .3, .9, .7];
animTime = 1;
iconBoxesPerRowCol = [3, 2];
targetColor = [30   250  20 255]';    % a light blue

iconPaths = cell(11,1);
for idx = 1 : 9
   iconPaths{idx} = [num2str(idx), '.png'] ;
end
iconPaths{10} = '0.png';
iconPaths{11} = 'bp.png';
numIcons = length(iconPaths);

