clc
clear;
clear Screen;
Screen('Preference', 'SkipSyncTests', 1);


%Size of screen
width=500;
height=500;

White=[255 255 255];
Black=[0 0 0];

window=Screen('OpenWindow',0,White,[0 0 width height]);
Car=InitializeBullet(200,200,100,150);

Screen('FillPoly', window, Black, Car);

Screen('Flip',window);