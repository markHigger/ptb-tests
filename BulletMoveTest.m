clc
clear;
clear Screen;
Screen('Preference', 'SkipSyncTests', 1);


%Size of screen
width=1000;
height=500;

White=[255 255 255];
Black=[0 0 0];

window=Screen('OpenWindow',0,White,[0 0 width height]);

for idx=1:255
Car=InitializeBullet(200+idx*2,200,100,150);

Screen('FillPoly', window, [100 255-idx idx], Car,0);

%change remainder for speed control
if rem(idx,1)==0
    Screen('Flip',window);
end
end