clc
clearvars;
clear Screen;
Screen('Preference', 'SkipSyncTests', 1);


%Size of screen
width=1000;
height=500;

White=[255 255 255];
Black=[0 0 0];

myimgfile='example.jpg';
ima=imread(myimgfile);



window=Screen('OpenWindow',0,Black,[0 0 width height]);
Screen('PutImage', window, ima,[0 0 100 100]); % put image on screen

Screen('Flip',window);
while KbCheck; end % clear keyboard queue
while ~KbCheck; end % wait for a key press
Screen('CloseAll')
