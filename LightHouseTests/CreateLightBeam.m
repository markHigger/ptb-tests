function  LightBeam  = CreateLightBeam(OriginX,OriginY,BoxCenter,LeftLightBoxLimit,RightLightBoxLimit,BoxHeight)
%Creates a light beam for the LightHouse Program *****This is a Test
%   Program and is NOT meant for final use
%Input
%   origin x and y the light origin positions
%   Box center is the center height of the light box
%   Right and Left Box Limits are the extent of the light box

%Sets Light beam origin
LightBeam(1,1)=OriginX;
LightBeam(1,2)=OriginY;

%BoxHmax/min are the max and min heights of the light box

BoxHmax=BoxCenter - BoxHeight/2;
BoxHmin=BoxCenter+BoxHeight/2;

%Sets Light Box coordintes, The rectagular coordinates are on the opisite
%side of the light light origin on the Y axis
if OriginY <= BoxCenter
    LightBeam(2,1)= LeftLightBoxLimit ;
    LightBeam(2,2)= BoxHmax;
    LightBeam(3,1)= LeftLightBoxLimit;
    LightBeam(3,2)= BoxHmin;
    LightBeam(4,1)=RightLightBoxLimit;
    LightBeam(4,2)= BoxHmin;
    LightBeam(5,1)=RightLightBoxLimit;
    LightBeam(5,2)=BoxHmax;
elseif OriginY > BoxCenter
    LightBeam(2,1)= LeftLightBoxLimit ;
    LightBeam(2,2)= BoxHmin;
    LightBeam(3,1)= LeftLightBoxLimit;
    LightBeam(3,2)= BoxHmax;
    LightBeam(4,1)=RightLightBoxLimit;
    LightBeam(4,2)= BoxHmax;
    LightBeam(5,1)=RightLightBoxLimit;
    LightBeam(5,2)=BoxHmin;
end

    
    





end

