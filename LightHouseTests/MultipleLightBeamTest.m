clc;
clearvars;
clear screen;

Screen('Preference', 'SkipSyncTests', 1); 

%Size of screen
width=1000;
height=700;

%Initialize colors
White=[255 255 255];
Black=[50 50 50];
PaleYellow=[255 255 153];
Yellow=[240 255 100];
Violet=[220 100 255];
%creating window
window=Screen('OpenWindow',0,White,[0 0 width height]);

%intial box placement
x11=0;
x12=100;
x21=150;
x22=250;
x31=300;
x32=400;
x41=450;
x42=550;


for idx=1:300
    if idx<=150
        Light1=CreateLightBeam(0,0,height/2,x11+idx/10,x12-idx/2,height/5);
    end
    Screen('FillPoly',window,Black,Light1);
    
    if idx <=100
        Light2=CreateLightBeam(0,height,height/2,x21-idx,x22,height/5);
    end
    Screen('FillPoly',window,PaleYellow,Light2);
    
    if idx <=200
        Light3=CreateLightBeam(width,height,height/2,x31,x32+idx,height/5);
    end
    Screen('FillPoly',window,Violet,Light3);
    
    
    if idx <=300
        Light4=CreateLightBeam(width,0,height/2,x41+idx,x42+idx,height/5);
    end
    Screen('FillPoly',window,Yellow,Light4);
    
    if rem(idx,3)==0
        Screen('Flip',window);
    end
end


