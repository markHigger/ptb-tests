% PTBdisplayObject.m
%
% PTBdisplayObject is a superclass for common PTB display features (iconBox: an array of images,
% textBox: a location for text, barGraph: a bar graph manually created from PTB objects)
% written by higger@ece.neu.edu 5-20-14

classdef PTBdisplayObject < handle
    properties
        position            %(4x1) PTB position
        windowPointer       % window pointer to active screen
        screenSizePixels    % [width,height] of active screen in pixels
    end
    
    methods
        function self = PTBdisplayObject(windowPointer,varargin)
            % constructor
            
            self.windowPointer = windowPointer;
            [self.screenSizePixels(1),self.screenSizePixels(2)] = WindowSize(self.windowPointer);
            self.screenSizePixels = self.screenSizePixels(:);
            
            p = inputParser;
            p.KeepUnmatched = true;    %doesn't cause error if all inputs don't match (sub/super class)
            p.addParameter('position',[], @(x)(isequal(size(x),[4,1])));
            p.addParameter('positionArguments',...
                {'center','pixelSizeXY',self.screenSizePixels},@iscell);
            p.parse(varargin{:});
            
            if ~isempty(p.Results.position)                
                self.position = p.Results.position;
            elseif ~isempty(p.Results.positionArguments)
                self.findPosition(p.Results.positionArguments{:});
            else
                error('Position or positionArguments must be specified for all PTBdisplayObjects')
            end
            
        end
        
        function varargout = findPosition(self,mode,varargin)
            % find position is a convenience functions which computes the PTB position of an object on the
            % screen given various ratios (see inputs below)
            %
            % REVISE: this whole method is ugly
            %
            % INPUT
            %   mode            = 'center', 'left', 'right', 'top', 'bottom', 'topLeft', 'topRight',
            %                   'bottomRight', 'bottomLeft', if direction not given assumed centered (ie 'left'
            %                   is on the left edge of monitor, centered vertically)
            %                       or
            %                   = 'float' object's center is defined by floatPixelSizeXY or floatSizeRatioXY
            %   pixelSizeXY     = (2x1) size of the object in pixels
            %   sizeRatioXY     = (2x1) size of the object relative to screen size
            %   floatPixelSizeXY= (2x1) location of object center in pixels
            %   floatSizeRatioXY= (2x1) location of object center relative to screen size
            %   boundingPosition= (4x1) position of bounding box (defaults full window)
            %   ratioAB         = (4x1) [xA yA xB yB] where A is top left
            %                      and B is bottom right of box (HILCPS)
            %   storeMode (opt) = 'set' (default), 'return'.  describes behavior of output
            % OUTPUT
            %   position        = (4x1) [a_x, a_y, b_x, b_y] position of object where a is top left corner and b
            %                     is bottom right (note PTB indexes (0,0) as the top left corner)
            
            %this function can be tested if copied outside of object (see end of function for
            %script)
            %self.screenSizePixels = [1920; 1080];   %for testing purposes outside of object
            
            p = inputParser;
            p.addRequired('mode',@(x)(sum(strcmp(x,{'center','left','right','top','bottom','topLeft', ...
                'topRight','bottomRight', 'bottomLeft','float', 'ratioAB'}))));
            p.addParameter('pixelSizeXY',[],@(x)(isequal(size(x),[2,1])));
            p.addParameter('sizeRatioXY',[],@(x)(isequal(size(x),[2,1])));
            p.addParameter('floatPixelSizeXY',[],@(x)(isequal(size(x),[2,1])));
            p.addParameter('floatSizeRatioXY',[],@(x)(isequal(size(x),[2,1])));
            p.addParameter('ratioAB', nan);
            p.addParameter('boundingPosition',[1;1;self.screenSizePixels],@(x)(isequal(size(x),[4,1])));
            p.addParameter('storeMode','set',@(x)(sum(strcmpi(x,{'set','return'}))));
            p.parse(mode,varargin{:});
            
            positionToSizeXY = @(pos)(pos(3:4)-pos(1:2));
            
            %determine pixelSizeXY from inputs
            if ~isempty(p.Results.pixelSizeXY)
                pixelSizeXY = p.Results.pixelSizeXY;
            elseif ~isempty(p.Results.sizeRatioXY)
                pixelSizeXY = floor(positionToSizeXY(p.Results.boundingPosition) .* p.Results.sizeRatioXY);
            elseif isnan(p.Results.ratioAB)
                error('either pixelSizeXY or sizeRatioXY is required ... how big is the object?');
            end
            
            % compute position is an anonymous function which handles x and y dimensions of position
            % seperately
            if ~strcmp(mode, 'ratioAB')
                computePositionXY = @(x,y)(findPixelRange('horz',x,pixelSizeXY,p.Results.boundingPosition) + ...
                    findPixelRange('vert',y,pixelSizeXY,p.Results.boundingPosition));
            end
            
            switch mode
                case 'center'
                    varargout{1} = computePositionXY('middle','middle');
                case 'left'
                    varargout{1} = computePositionXY('first','middle');
                case 'right'
                    varargout{1} = computePositionXY('last','middle');
                case 'top'
                    varargout{1} = computePositionXY('middle','first');
                case 'bottom'
                    varargout{1} = computePositionXY('middle','last');
                case 'topLeft'
                    varargout{1} = computePositionXY('first','first');
                case 'topRight'
                    varargout{1} = computePositionXY('last','first');
                case 'bottomRight'
                    varargout{1} = computePositionXY('last','last');
                case 'bottomLeft'
                    varargout{1} = computePositionXY('first','last');
                case 'ratioAB'
                    if isnan(p.Results.ratioAB)
                        error('ratioAB input required in ratioAB mode');
                    end
                    temp = [self.screenSizePixels; self.screenSizePixels];
                    varargout{1} = p.Results.ratioAB(:) .* temp(:);
                case 'float'
                    if ~isempty(p.Results.floatPixelSizeXY)
                        floatPixelSizeXY = p.Results.pixelSizeXY;
                    elseif ~isempty(p.Results.floatSizeRatioXY)
                        floatPixelSizeXY = positionToSizeXY(p.Results.boundingPosition) .* p.Results.floatSizeRatioXY;
                    else
                        error('either floatPixelSizeXY or floatSizeRatioXY is required for flaot mode');
                    end
                    
                    varargout{1} = zeros(4,1);
                    varargout{1}(1:2) = p.Results.boundingPosition(1:2)+floor(floatPixelSizeXY - pixelSizeXY / 2);
                    varargout{1}(3:4) = varargout{1}(1:2) + pixelSizeXY - [1; 1];
                    
                    outOfRange = @(x,range)(sum(sum(x < range(1)))||sum(sum(x > range(2))));
                    if outOfRange(varargout{1}([1,3]),p.Results.boundingPosition([1,3])) || ...  %horizontal
                            outOfRange(varargout{1}([2,4]),p.Results.boundingPosition([2,4]))    %vertical
                        warning('float positioning has placed object outside of bounding window!');
                    end
            end
            
            if strcmpi(p.Results.storeMode,'set')
                self.position = varargout{1};
            end
            
            function pixelRangePTBposition = findPixelRange(horzVert, firstMiddleLast, pixelSizeXY,boundingPosition)
                % this subfunction works on either the horizontal or vertical dimensions of a 4x1
                % PTB position vector to find appropriate range of pixels in appropriate dimension
                % INPUT
                %   horzVert        = 'horz' or 'vert'
                %   firstMiddleLast = 'first', 'middle' or 'last', determines which pixel ragne is
                %                   selected 
                %   pixelSizeXY     =(2x1) size of object requested in pixels [x,y] format
                %   boudningPosition= range from which to select the 'first', 'middle', or 'last' of
                % OUTPUT
                %   pixelRangePTBposition = [4x1] PTB position [x1 y1 x2 y2]' with horizontal or
                %                    vertical pixel places (given by horzVert) computed, others are 0
                
                pixelRangePTBposition = zeros(4,1);
                switch horzVert
                    case 'horz'
                        activeIdx = [1,3];
                        pixelSize = floor(pixelSizeXY(1));
                        minVal = boundingPosition(1);
                        maxVal = boundingPosition(3);
                    case 'vert'
                        activeIdx = [2,4];
                        pixelSize = floor(pixelSizeXY(2));
                        minVal = boundingPosition(2);
                        maxVal = boundingPosition(4);
                end
                
                switch firstMiddleLast
                    case 'first'
                        % choose the range corresponding to first pixelSize locations
                        pixelRangePTBposition(activeIdx) = floor([minVal,minVal+pixelSize-1]);
                    case 'middle'
                        % choose the range corresponding to middle most pixelSize locations
                        startPoint = floor(minVal+(maxVal-minVal-pixelSize) / 2);
                        pixelRangePTBposition(activeIdx) = [startPoint, startPoint+ pixelSize - 1];
                    case 'last'
                        % choose the range corresponding to last pixelSize locations
                        pixelRangePTBposition(activeIdx) = floor([maxVal-pixelSize+1,maxVal]);
                end
                
            end
            
            %%% findPositionTest.m
            % % findPosition runs as a function if you uncomment the hardcoded "self.screenSizePixels
            % % = ..." line as well as the "self" input parameter.
            % clearvars; clc;
            %
            % mode = {'center', 'left', 'right', 'top', 'bottom', 'topLeft', 'topRight','bottomRight', 'bottomLeft'};
            % sizeRatioXY = [.2;.45];
            % floatSizeRatioXY = [.7;.6];
            % boundingPosition = [500, 1, 1000, 900]';
            %
            % %% without bounding box (full screen used)
            % figure;
            % for ii = 1 : length(mode)
            %     position = findPosition(mode{ii},'sizeRatioXY',sizeRatioXY);
            %     rectangle('Position',[1,1,1920,1080])
            %     hold on;
            %     [x,y] = meshgrid(position([1,3]),abs(position([2,4])-1080));
            %     scatter(x(:),y(:));
            %     text(1920/2,1080/2,[mode{ii}])
            %     pause
            %     clf
            % end
            % position = findPosition('float','sizeRatioXY',sizeRatioXY,'floatSizeRatioXY',floatSizeRatioXY);
            % rectangle('Position',[1,1,1920,1080])
            % hold on;
            % [x,y] = meshgrid(position([1,3]),abs(position([2,4])-1080));
            % scatter(x(:),y(:));
            % text(1920/2,1080/2,'float');
            % pause;
            % close all;
            %
            % %% and with bounding box ...
            % for ii = 1 : length(mode)
            %     position = findPosition(mode{ii},'sizeRatioXY',sizeRatioXY,...
            %         'boundingPosition',boundingPosition);
            %     rectangle('Position',[1,1,1920,1080])
            %     hold on;
            %         rectangle('Position',[boundingPosition(1),1080-boundingPosition(4),boundingPosition(3)-boundingPosition(1),boundingPosition(4)-boundingPosition(2)])
            %     [x,y] = meshgrid(position([1,3]),abs(position([2,4])-1080));
            %     scatter(x(:),y(:));
            %     text(1920/2,1080/2,[mode{ii}])
            %     pause
            %     clf
            % end
        end
    end
    
end