classdef IconTestObj <handle
    properties
        Icons               %(1 x numIcons cell array) list of icons
        iconPosition        %(2 x numPossibleIcons) PTB position of each icon in array
        numIcons            %(scalar) number of Icons
        BoxWidth            %(2 x 1) Size of the Icon Box
    end
    
    methods
        function self=IconTestObj(Files,boxWidth)
            
            FilesSize=size(Files);
            self.numIcons=FilesSize(1);
            for IconIdx=1:self.numIcons
                self.Icons{IconIdx}= imread(Files{IconIdx});
            end
            
            self.BoxWidth=boxWidth;
            for positionIdx= 1:self.numIcons
                self.iconPosition(positionIdx,1)=(positionIdx-1)*self.BoxWidth/self.numIcons;
                self.iconPosition(positionIdx,2)=positionIdx*self.BoxWidth/self.numIcons;
            end
        end
    end
        
end
